# Translation of kcm_kded to Northern Sami
#
# Børre Gaup <boerre@skolelinux.no>, 2003, 2004, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kcmkded\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-28 00:39+0000\n"
"PO-Revision-Date: 2007-12-24 01:57+0100\n"
"Last-Translator: Børre Gaup <boerre@skolelinux.no>\n"
"Language-Team: Northern Sami <l10n-no@lister.huftis.org>\n"
"Language: se\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: kcmkded.cpp:115
#, kde-format
msgid "Failed to stop service: %1"
msgstr ""

#: kcmkded.cpp:117
#, kde-format
msgid "Failed to start service: %1"
msgstr ""

#: kcmkded.cpp:124
#, kde-format
msgid "Failed to stop service."
msgstr ""

#: kcmkded.cpp:126
#, kde-format
msgid "Failed to start service."
msgstr ""

#: kcmkded.cpp:224
#, kde-format
msgid "Failed to notify KDE Service Manager (kded6) of saved changed: %1"
msgstr ""

#: ui/main.qml:40
#, kde-format
msgid ""
"The background services manager (kded6) is currently not running. Make sure "
"it is installed correctly."
msgstr ""

#: ui/main.qml:50
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""

#: ui/main.qml:60
#, kde-format
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded6) was restarted to apply your changes."
msgstr ""

#: ui/main.qml:108
#, kde-format
msgid "All Services"
msgstr ""

#: ui/main.qml:109
#, kde-format
msgctxt "List running services"
msgid "Running"
msgstr ""

#: ui/main.qml:110
#, kde-format
msgctxt "List not running services"
msgid "Not Running"
msgstr ""

#: ui/main.qml:147
#, kde-format
msgid "Startup Services"
msgstr "Álggahanbálvalusat"

#: ui/main.qml:148
#, kde-format
msgid "Load-on-Demand Services"
msgstr "Go-dárbbašuvvo-bálvalusat"

#: ui/main.qml:167
#, kde-format
msgctxt "@action:button %1 service name"
msgid "Disable automatically loading %1 on startup"
msgstr ""

#: ui/main.qml:167
#, kde-format
msgctxt "@action:button %1 service name"
msgid "Enable automatically loading %1 on startup"
msgstr ""

#: ui/main.qml:168
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr ""

#: ui/main.qml:212
#, kde-format
msgid "Not running"
msgstr "Ii jođus"

#: ui/main.qml:213
#, kde-format
msgid "Running"
msgstr "Jođus"

#: ui/main.qml:233
#, kde-format
msgctxt "@action:button %1 service name"
msgid "Stop %1"
msgstr ""

#: ui/main.qml:233
#, kde-format
msgctxt "@action:button %1 service name"
msgid "Start %1"
msgstr ""

#: ui/main.qml:236
#, kde-format
msgid "Stop Service"
msgstr ""

#: ui/main.qml:236
#, kde-format
msgid "Start Service"
msgstr ""
